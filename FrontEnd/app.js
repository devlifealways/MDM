import React from 'react'
import ReactDOM from 'react-dom'
import 'bootstrap'


const css = require('./src/style/app.scss')

class Header extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            value: null
        }
    }
    render() {
        return (
            <h1></h1>
        )
    }
}


class CommentBox extends React.Component {
    render() {
        return (
            <div className="container">
                This is a real container !
            </div>
        )
    }
}

ReactDOM.render(
    <CommentBox />,
    document.getElementById('component')
);

export default Header;
